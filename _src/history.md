---
title: Historia
---

{{ page.title }}
================

Koło Naukowe Fizyków Uniwersytetu Śląskiego w Katowicach powstało w 1970r. Przez 45 lat
przeżywało ono swoje wzloty i upadki. Niestety w okresie 1990 - 2001 zainteresowanie
studentów dodatkową działalnością w Kole było na tyle nikłe, że okres ten w historii naszej
organizacji został ochrzczony mianem "uśpienia". W roku 2001 Koło Naukowe Fizyków zostało
reaktywowane przez grupę młodych i ambitnych studentów fizyki.

Począwszy od swojej reaktywacji w 2001 roku, Koło organizuje coroczną
<a href="http://okknf.smcebi.edu.pl/">Ogólnopolską Konferencję Kół Naukowych Fizyków</a>.
Stwarza ona możliwości wymiany myśli i integracji zarówno z młodymi fizykami zrzeszonymi
w podobnych do KNF organizacjach, jak i przedstawicielami świata nauki.

Członkowie zarządu
------------------

### 2016/06 - ...

* Kamil Wójcik (prezes)
* Zbigniew Grzanka (v-ce prezes)
* Kamil Simiński (sekretarz)

### 2009/04 - 2010/03

* Aleksander Ciba (prezes)
* Monika Pieńkos (v-ce prezes ds. naukowych)
* Sylwia Miernik (v-ce prezes ds. finansowych)


### 2008/04 - 2009/03

* Piotr Szaflik (prezes)
* Mikołaj Karawacki (v-ce prezes ds. naukowych)
* Wojciech Ciszek (v-ce prezes ds. finansowych)


### 2007/04 - 2008/04

* Łukasz Połacik (prezes)
* Mikołaj Karawacki (v-ce prezes ds. naukowych)
* Andrzej Wilczek (v-ce prezes ds. finansowych)


### 2006/04 - 2007/04

* Michał Januszewski (prezes)
* Katarzyna Bartuś (v-ce prezes ds. naukowych)
* Andrzej Wilczek (v-ce prezes ds. finansowych)


### 2005/04 - 2006/04

* Andrzej Ptok (prezes)
* Katarzyna Bartuś (v-ce prezes ds. naukowych)
* Agnieszka Grzanka (v-ce prezes ds. finansowych)

---
title: Galeria
permalink: /gallery/
---

{{ page.title }}
================

Poniżej znajdują się linki do galerii zdjęć zebranych podczas kilkunastu już lat istnienia KNF.

2014
----
* [Toruńskie Spotkania z Fizyką 2014 (autorzy zdjęć: organizatorzy TSzF)](https://plus.google.com/photos/115197416195170478269/albums/6153907559618638513)

2011
----
* [Święto Liczby Pi 2011](https://plus.google.com/photos/115197416195170478269/albums/6146120468188312097)

2009
----
* [Święto Liczby Pi 2009](https://plus.google.com/photos/115197416195170478269/albums/6146110548373631777)

2007
----
* [V OSKNF (16-18 XI 2007, Lublin)](https://plus.google.com/photos/115197416195170478269/albums/6152887765027848273)
* [XXXIX Zjazd Fizyków Polskich (9-14 IX 2007, Szczecin)](https://plus.google.com/photos/115197416195170478269/albums/6152907627225343457)

2006
----
* [IV OSKNF (17-19 XI 2006, Wrocław)](https://plus.google.com/photos/115197416195170478269/albums/6152895333337821217)
* [Ogólnopolski Konkurs na Projekt Multimedialny z Fizyki 2006](https://plus.google.com/photos/115197416195170478269/albums/6152903619417569441)
* V OKKNF "Piknik Naukowy 2006" (20-23.IV.2006, Cieszyn):
    - Maciej Stodolny, PG
    - Agata Dragan, UAM
    - Sebastan Szwarc, UAM
    - Marta Chłopecka, UG
    - Katarzyna Bartuś, UŚ
* Dyskusja panelowa "Oblicza fizyki" 2006:
    - [Katarzyna Bartuś](https://plus.google.com/photos/115197416195170478269/albums/6146147698067922593)
    - [Łukasz Lupa](https://plus.google.com/photos/115197416195170478269/albums/6146150692217638961)

2005
----
* [OSKNF (18-20 XI 2005, Kraków)](https://plus.google.com/photos/115197416195170478269/albums/6152884034297759345)
* [Zjazd Fizyków Polskich (11-16 IX 2005, Warszawa)](https://plus.google.com/photos/115197416195170478269/albums/6152911623058525841)
* [Ogólnopolski Konkurs na Projekt Multimedialny z Fizyki 2005](https://plus.google.com/photos/115197416195170478269/albums/6152920855335073521)
* IV OKKNF "Piknik Naukowy 2005" (22-24.IV.2005, Ustroń Jaszowiec):
    - Katarzyna Bartuś, UŚ
    - Agnieszka Pleban, UŚ
    - Politechnika Łódzka
    - Akademia Świętokrzyska
    - Akademia Pedagogiczna w Krakowie
* [Wyprawa do kopalni Polkowice-Sieroszowice](https://plus.google.com/photos/115197416195170478269/albums/6146144527088362737)
* [Festiwal Nauki 2005](https://plus.google.com/photos/115197416195170478269/albums/6146136208037407329)

2004
----
* [III OKKNF "Piknik Naukowy 2004" (13-16.V.2004, Głuchołazy)](https://goo.gl/photos/6xmVnX6yyoxDCHwu7)

2003
----
* [Zimowa Sesja KNF w Węgierskiej Górce](https://plus.google.com/photos/115197416195170478269/albums/6152900346399528369)
* [II OSKNF (23-26 X 2003, Kraków)](https://plus.google.com/photos/115197416195170478269/albums/6152881526716515025)
* [II OKKNF "Piknik Naukowy 2003" (24-27.IV.2003, Zwardoń)](https://goo.gl/photos/xFGZ1RbANn6pPSzD7)

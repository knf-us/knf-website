---
title: Wydarzenia
permalink: /events/
---

{{ page.title }}
================

{% assign conferences = (site.data.conferences | sort: 'date' | reverse) %}
{% assign events = (site.data.events | sort: 'date' | reverse) %}

Konferencje
-----------

<table class="table table-striped table-hover table-responsive">
    <thead>
        <tr>
            <th class="text-center">Data</th>
            <th class="text-center">Nazwa</th>
        </tr>
    </thead>
    <tbody>
    {% for conference in conferences %}
        <tr>
            <td class="text-center">{{ conference.date | date: "%d.%m.%Y" }}{% if conference.end_date %} - {{ conference.end_date | date: "%d.%m.%Y" }}{% endif %}</td>
            <td class="text-center">{{ conference.name }}</td>
        </tr>
    {% endfor %}
    </tbody>
</table>

Inne wydarzenia
---------------

<table class="table table-striped table-hover table-responsive">
    <thead>
        <tr>
            <th class="text-center">Data</th>
            <th class="text-center">Wydarzenie</th>
        </tr>
    </thead>
    <tbody>
    {% for event in events %}
        <tr>
            <td class="text-center">{{ event.date | date: "%d.%m.%Y" }}{% if event.end_date %} - {{ event.end_date | date: "%d.%m.%Y" }}{% endif %}</td>
            <td class="text-center">{{ event.name }}</td>
        </tr>
    {% endfor %}
    </tbody>
</table>

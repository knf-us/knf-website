---
title: O nas
---

Kim jesteśmy?
=============

Koło Naukowe Fizyków UŚ to grupa studentów Uniwersytetu Śląskiego, których łączy jedna
wspólna cecha: fascynacja fizyką. Zgodnie z wyznawaną przez nas ideą - "Umysł ścisły, nie
znaczy ciasny" w naszych dyskusjach równie często pojawiają się wątki związane z kulturą,
sztuką, polityką i wszystkimi tymi sprawami, które zwrócą naszą uwagę. Chociaż większość
członków koła studiuje fizykę, są wśród nas także studenci innych kierunków. Wielu studiuje
jednocześnie na dwóch wydziałach lub jest studentami MISMP-u.

Co robimy?
==========

W KNF-ie zajmujemy się wieloma mniej lub bardziej poważnymi rzeczami. Organizujemy
[konferencje](http://okknf.smcebi.edu.pl/) oraz prowadzimy ciekawe projekty.
Dzielimy się swoimi przemyśleniami i wynikami swojej pracy na seminariach. Staramy się
popularyzować fizykę wśród studentów, uczniów szkół średnich oraz ogółu społeczeństwa.
Czasem organizujemy również wycieczki do różnych instytucji naukowych.

Chętnie pomagamy i doradzamy wszystkim zainteresowanym w przypadku problemów związanych
z fizyką i informatyką. Popieramy również idee wolnego oprogramowania oraz wolnego dostępu
do wiedzy. Od roku akademickiego 2014/2015 członkowie KNF biorą aktywny udział w redakcji
[[Macierzatora]](http://knm.katowice.pl/macierzator.php).

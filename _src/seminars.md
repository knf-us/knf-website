---
title: Seminaria
permalink: /seminars/
---

{{ page.title }}
================

{% assign seminars_sorted_by_date = (site.data.seminars | sort: 'date' | reverse) %}

<table class="table table-striped table-hover table-responsive">
    <thead>
        <tr>
            <th class="text-center">Data</th>
            <th class="text-center">Temat</th>
            <th class="text-center">Prelegent</th>
        </tr>
    </thead>
    <tbody>
    {% for seminar in seminars_sorted_by_date %}
        <tr>
            <td class="text-center">{{ seminar.date | date: "%d.%m.%Y" }}</td>
            <td class="text-center">{{ seminar.subject }}</td>
            <td class="text-center">{{ seminar.author }}</td>
        </tr>
    {% endfor %}
    </tbody>
</table>

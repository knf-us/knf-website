---
title: Warsztaty
permalink: /workshops/
---

{{ page.title }}
================

{% assign workshops_sorted_by_date = (site.data.workshops | sort: 'date' | reverse) %}

<table class="table table-striped table-hover table-responsive">
    <thead>
        <tr>
            <th class="text-center">Data</th>
            <th class="text-center">Temat</th>
            <th class="text-center">Prowadzący</th>
        </tr>
    </thead>
    <tbody>
    {% for workshop in workshops_sorted_by_date %}
        <tr>
            <td class="text-center">{{ workshop.date | date: "%d.%m.%Y" }}</td>
            <td class="text-center">{{ workshop.subject }}</td>
            <td class="text-center">{{ workshop.author }}</td>
        </tr>
    {% endfor %}
    </tbody>
</table>

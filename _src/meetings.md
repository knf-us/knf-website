---
title: Spotkania
permalink: /meetings/
---

{% assign meetings_sorted_by_date = (site.data.meetings | sort: 'date' | reverse) %}

{{ page.title }}
================

<table class="table table-striped table-hover table-responsive">
    <thead>
        <tr>
            <th class="text-center">Data</th>
            <th class="text-center">Temat</th>
        </tr>
    </thead>
    <tbody>
    {% for meeting in meetings_sorted_by_date %}
        <tr>
            <td class="text-center">{{ meeting.date | date: "%d.%m.%Y" }}</td>
            <td class="text-center">{{ meeting.subject }}</td>
        </tr>
    {% endfor %}
    </tbody>
</table>
